import requests
from PyQt5.QtCore import QThread, pyqtSignal
from bs4 import BeautifulSoup

from modules import browsercookie

USER_COOKIES = browsercookie.load()
# for c in USER_COOKIES:
#     if c.name == "language":
#         c.value = "en"
#         USER_COOKIES.set_cookie(c)


class UserInfoThread(QThread):
    update_signal = pyqtSignal(int)
    username = None
    days_at_sea = None

    def __init__(self, username, **kwargs):
        super().__init__(**kwargs)
        self.username = username

    def run(self):
        req = requests.get(f"https://www.seaofthieves.com/community/forums/user/{self.username}", cookies=USER_COOKIES)
        soup = BeautifulSoup(req.text, features="html.parser")
        try:
            self.days_at_sea = int(soup.find_all("div", class_="stat")[1].find("div").text)
            self.update_signal.emit(self.days_at_sea)
        except:
            self.days_at_sea = None
