import json
import os
import sys


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        base_path = sys._MEIPASS
    except:
        # base_path = os.path.abspath(".")
        base_path = ""

    return os.path.join(base_path, relative_path)


class Settings:
    friends = []
    enemies = []
    uworld_addr = 0x729942
    gobject_addr = 0x1681524
    gname_addr = 0x15956f8

    def __init__(self):
        if os.path.exists("settings.json"):
            self.load_settings()
        self.save_settings()

    def load_settings(self):
        with open("settings.json", "r") as f:
            data = json.load(f)
            for key in data:
                if hasattr(self, key):
                    setattr(self, key, data[key])

    def save_settings(self):
        with open("settings.json", "w") as f:
            json.dump({
                "friends": self.friends,
                "enemies": self.enemies,
                "uworld_addr": self.uworld_addr,
                "gobject_addr": self.gobject_addr,
                "gname_addr": self.gname_addr
            }, f)

    def set_friends(self, friends):
        self.friends = friends
        self.save_settings()

    def set_enemies(self, enemies):
        self.enemies = enemies
        self.save_settings()

    def set_base_addresses(self, uworld, gobject, gname):
        self.uworld_addr = uworld
        self.gobject_addr = gobject
        self.gname_addr = gname
        self.save_settings()


with open(resource_path("offsets.json")) as infile:
    OFFSETS = json.load(infile)

SETTINGS = Settings()

GLOBAL_INSTANCE_DATA = {
    "local_player_name": None
}
