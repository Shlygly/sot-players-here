"""
@Author https://github.com/DougTheDruid
@Source https://github.com/DougTheDruid/SoT-ESP-Framework
For community support, please contact me on Discord: DougTheDruid#2784
"""
import struct

from Crew import Crew
from PlayerState import PlayerState
from memory_helper import ReadMemory
from helpers import OFFSETS, SETTINGS, GLOBAL_INSTANCE_DATA


class SoTMemoryReader:
    """
    Wrapper class to handle reading data from the game, parsing what is
    important, and returning it to be shown by pygame
    """

    def __init__(self):
        """
        Upon initialization of this object, we want to find the base address
        for the SoTGame.exe, then begin to load in the static addresses for the
        uWorld, gName, gObject, and uLevel objects.

        We also poll the local_player object to get a first round of coords.
        When running read_actors, we update the local players coordinates
        using the camera-manager object

        Also initialize a number of class variables which help us cache some
        basic information
        """
        self.rm = ReadMemory("SoTGame.exe")
        self.base_address = self.rm.base_address

        self.load_party_data()

        self.my_coords['fov'] = 90

        self.actor_name_map = {}
        self.crews = {}
        self.local_player_name = None

    def load_party_data(self):

        u_world_offset = self.rm.read_ulong(self.base_address + SETTINGS.uworld_addr + 3)
        u_world = self.base_address + SETTINGS.uworld_addr + u_world_offset + 7
        self.world_address = self.rm.read_ptr(u_world)

        g_name_offset = self.rm.read_ulong(self.base_address + SETTINGS.gname_addr + 3)
        self.g_name = self.rm.read_ptr(self.base_address + SETTINGS.gname_addr
                                       + g_name_offset + 7)

        g_objects_offset = self.rm.read_ulong(self.base_address + SETTINGS.gobject_addr + 2)
        g_objects = self.base_address + SETTINGS.gobject_addr + g_objects_offset + 22
        self.g_objects = self.rm.read_ptr(g_objects)

        self.u_level = self.rm.read_ptr(self.world_address +
                                        OFFSETS.get('UWorld.PersistentLevel'))

        u_local_player = self._load_local_player()

        self.player_controller = self.rm.read_ptr(
            u_local_player + OFFSETS.get('ULocalPlayer.PlayerController')
        )

        acknowledged_pawn = self.rm.read_ptr(
            self.player_controller + OFFSETS.get('APlayerController.AcknowledgedPawn')
        )

        try:
            self.local_player_name = PlayerState.from_memory(self.rm, self.rm.read_ptr(
                acknowledged_pawn + OFFSETS.get('APawn.PlayerState')
            )).name
            if GLOBAL_INSTANCE_DATA["local_player_name"] != self.local_player_name:
                GLOBAL_INSTANCE_DATA["local_player_name"] = self.local_player_name
        except:
            pass

        self.my_coords = self._coord_builder(u_local_player)

    def _load_local_player(self) -> int:
        """
        Returns the local player object out of uWorld.UGameInstance.
        Used to get the players coordinates before reading any actors
        :rtype: int
        :return: Memory address of the local player object
        """
        game_instance = self.rm.read_ptr(
            self.world_address + OFFSETS.get('UWorld.OwningGameInstance')
        )
        local_player = self.rm.read_ptr(
            game_instance + OFFSETS.get('UGameInstance.LocalPlayers')
        )
        return self.rm.read_ptr(local_player)

    def _coord_builder(self, actor_address: int, offset=0x78, camera=True,
                       fov=False) -> dict:
        """
        Given a specific actor, loads the coordinates for that actor given
        a number of parameters to define the output
        :param int actor_address: Actors base memory address
        :param int offset: Offset from actor address to beginning of coords
        :param bool camera: If you want the camera info as well
        :param bool fov: If you want the FoV info as well
        :rtype: dict
        :return: A dictionary contianing the coordinate information
        for a specific actor
        """
        if fov:
            actor_bytes = self.rm.read_bytes(actor_address + offset, 44)
            unpacked = struct.unpack("<ffffff16pf", actor_bytes)
        else:
            actor_bytes = self.rm.read_bytes(actor_address + offset, 24)
            unpacked = struct.unpack("<ffffff", actor_bytes)

        coordinate_dict = {"x": unpacked[0]/100, "y": unpacked[1]/100,
                           "z": unpacked[2]/100}
        if camera:
            coordinate_dict["cam_x"] = unpacked[3]
            coordinate_dict["cam_y"] = unpacked[4]
            coordinate_dict["cam_z"] = unpacked[5]
        if fov:
            coordinate_dict['fov'] = unpacked[7]

        return coordinate_dict

    def _read_name(self, actor_id: int) -> str:
        """
        Looks up an actors name in the g_name DB based on the actor ID provided
        :param int actor_id: The ID for the actor we want to find the name of
        :rtype: str
        :return: The name for the actor
        """
        name_ptr = self.rm.read_ptr(self.g_name + int(actor_id / 0x4000) * 0x8)
        name = self.rm.read_ptr(name_ptr + 0x8 * int(actor_id % 0x4000))
        return self.rm.read_string(name + 0x10, 64)

    def read_actors(self):
        """
        Main game loop. Is responsible for determining how many actors to scan
        and calling assisting functions to interpret data about said actors.
        Stores all relevent information in class variables
        """
        actor_raw = self.rm.read_bytes(self.u_level + 0xa0, 0xC)
        actor_data = struct.unpack("<Qi", actor_raw)

        new_crews = {}
        for x in range(0, actor_data[1]):
            # We start by getting the ActorID for a given actor, and comparing
            # that ID to a list of "known" id's we cache in self.actor_name_map
            raw_name = ""
            actor_address = self.rm.read_ptr(actor_data[0] + (x * 0x8))
            actor_id = self.rm.read_int(
                actor_address + OFFSETS.get('AActor.actorId')
            )
            if actor_id not in self.actor_name_map and actor_id != 0:
                try:
                    raw_name = self._read_name(actor_id)
                    self.actor_name_map[actor_id] = raw_name
                except Exception as e:
                    print(str(e))
            elif actor_id in self.actor_name_map:
                raw_name = self.actor_name_map.get(actor_id)

            # Ignore anything we cannot find a name for
            if not raw_name:
                continue

            elif "CrewService" in raw_name:
                list_address = self.rm.read_ptr(actor_address + OFFSETS.get("ACrewService.Crews"))
                crew_count = self.rm.read_int(actor_address + OFFSETS.get("ACrewService.Crews") + 8)
                for crew_index in range(crew_count):
                    crew_address = list_address + 0x90 * crew_index
                    crew = Crew.from_memory(self.rm, crew_address)
                    new_crews[crew.id] = crew

        self.crews = new_crews
