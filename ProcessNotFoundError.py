class ProcessNotFoundError(Exception):
    def __init__(self, proc_name):
        super().__init__(f"Cannot find executable with name: {proc_name}")
