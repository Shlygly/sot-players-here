from enum import Enum

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap

from UserInfo import UserInfoThread
from helpers import OFFSETS, resource_path


class PlayerState:
    def __init__(self, player_name, player_ping, player_activity, player_gender, player_ethnicity):
        self.name = player_name
        self.ping = player_ping
        self.activity = PlayerActivity(player_activity)
        self.gender = PlayerGender(player_gender)
        self.ethnicity = PlayerEthnicity(player_ethnicity)
        self.user_info_thread = UserInfoThread(self.name)

    def set_info_update(self, on_info_update):
        self.user_info_thread.update_signal.connect(on_info_update)
        self.user_info_thread.start()

    def get_activity_icon(self):
        if self.activity == PlayerActivity.Digging:
            index = 1
        elif self.activity in (PlayerActivity.CarryingBooty, PlayerActivity.CarryingBooty_END):
            index = 2
        elif self.activity in (PlayerActivity.Wheel, PlayerActivity.Wheel_END):
            index = 3
        elif self.activity in (PlayerActivity.Sails, PlayerActivity.Sails_END):
            index = 4
        elif self.activity in (PlayerActivity.Capstan, PlayerActivity.Capstan_END):
            index = 5
        elif self.activity in (PlayerActivity.Cannon, PlayerActivity.Cannon_END):
            index = 6
        elif self.activity in (PlayerActivity.Harpoon, PlayerActivity.Harpoon_END):
            index = 7
        elif self.activity == PlayerActivity.Repairing:
            index = 8
        elif self.activity == PlayerActivity.Bailing:
            index = 9
        elif self.activity == PlayerActivity.EmptyingBucket:
            index = 10
        elif self.activity == PlayerActivity.Dousing:
            index = 11
        elif self.activity == PlayerActivity.LoseHealth:
            index = 12
        elif self.activity in (PlayerActivity.Dead, PlayerActivity.Dead_END):
            index = 13
        else:
            index = 0
        return QPixmap(resource_path("images/activity-sprites.png")) \
            .copy(40 * index, 0, 40, 40) \
            .scaled(36, 36, Qt.KeepAspectRatio, Qt.SmoothTransformation)

    @staticmethod
    def from_memory(reader, actor_address):
        player_name = reader.read_name_string(reader.read_ptr(
            actor_address + OFFSETS.get('APlayerState.PlayerName')
        ))
        player_ping = reader.read_bytes(
            actor_address + OFFSETS.get('APlayerState.Ping'), 1
        )[0]
        player_activity = reader.read_bytes(
            actor_address + OFFSETS.get('APlayerState.PlayerActivity'), 1
        )[0]
        player_gender = reader.read_bytes(
            actor_address + OFFSETS.get('AAthenaPlayerState.PirateDesc.Gender'), 1
        )[0]
        player_ethnicity = reader.read_bytes(
            actor_address + OFFSETS.get('AAthenaPlayerState.PirateDesc.Ethnicity'), 1
        )[0]
        return PlayerState(
            player_name,
            player_ping,
            player_activity,
            player_gender,
            player_ethnicity
        )


class PlayerActivity(Enum):
    No = 0
    Bailing = 1
    Cannon = 2
    Cannon_END = 3
    Capstan = 4
    Capstan_END = 5
    CarryingBooty = 6
    CarryingBooty_END = 7
    Dead = 8
    Dead_END = 9
    Digging = 10
    Dousing = 11
    EmptyingBucket = 12
    Harpoon = 13
    Harpoon_END = 14
    LoseHealth = 15
    Repairing = 16
    Sails = 17
    Sails_END = 18
    Wheel = 19
    Wheel_END = 20
    MAX = 21
    EPlayerActivityType_MAX = 22


class PlayerGender(Enum):
    Unspecified = 0
    Male = 1
    Female = 2
    EIPGGender_max = 3


class PlayerEthnicity(Enum):
    Unspecified = 0
    Asian = 1
    Black = 2
    White = 3
    EIPGEthnicity_max = 4
