from enum import Enum

from PlayerState import PlayerState


class Crew:
    def __init__(self, crew_id, players, ship_type):
        self.id = crew_id
        self.players = players
        self.ship_type = ShipType(ship_type)

    @staticmethod
    def from_memory(reader, actor_address):
        crew_id = ''.join(hex(b)[2:] for b in reader.read_bytes(actor_address, 0x10))

        players = []
        members_list_address = reader.read_ptr(actor_address + 0x20)
        members_count = reader.read_int(actor_address + 0x28)
        for member_index in range(members_count):
            member_address = reader.read_ptr(members_list_address + 0x8 * member_index)
            player = PlayerState.from_memory(reader, member_address)
            players.append(player)

        ship_size = reader.read_int(actor_address + 0x30 + 0x30)

        return Crew(
            crew_id,
            players,
            ship_size
        )


class ShipType(Enum):
    Sloop = 2
    Brigantine = 3
    Galleon = 4
