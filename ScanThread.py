from PyQt5.QtCore import QThread, pyqtSignal

from helpers import SETTINGS


UWORLD_PATTERN = "48 8B 05 ? ? ? ? 48 8B 88 ? ? ? ? 48 85 C9 74 06 48 8B 49 70"
GOBJECT_PATTERN = "89 0D ? ? ? ? 48 8B DF 48 89 5C 24"
GNAME_PATTERN = "48 8B 1D ? ? ? ? 48 85 DB 75 ? B9 08 04 00 00"


class ScanThread(QThread):
    update_scan_signal = pyqtSignal(int)
    finish_scan_signal = pyqtSignal(dict)

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, *kwargs)
        self.app = app

    def run(self):
        self.update_scan_signal.emit(1)
        uworld_address = self.app.sot_memory.rm.search_pattern(UWORLD_PATTERN)

        self.update_scan_signal.emit(2)
        gobject_address = self.app.sot_memory.rm.search_pattern(GOBJECT_PATTERN)

        self.update_scan_signal.emit(3)
        gname_address = self.app.sot_memory.rm.search_pattern(GNAME_PATTERN)

        SETTINGS.set_base_addresses(uworld_address, gobject_address, gname_address)

        self.finish_scan_signal.emit({
            "uworld": uworld_address,
            "gobject": gobject_address,
            "gname": gname_address
        })

        self.app.on_game_refresh_press()
