import time

from PyQt5.QtCore import QThread, pyqtSignal

MAX_SWAP_COUNT = 10


class MainThread(QThread):
    set_crews_signal = pyqtSignal(dict)

    def __init__(self, app, **kwargs):
        super().__init__(**kwargs)
        self.app = app
        self.swap_count = 0

    def run(self):
        end = False
        while not end:
            if self.app.sot_memory:
                self.app.sot_memory.read_actors()
                self.app.sot_memory.load_party_data()
                if self.app.sot_memory.crews:
                    self.app.set_form("main")
                    self.set_crews_signal.emit(dict(self.app.sot_memory.crews))
                    self.swap_count = 0
                elif self.swap_count > MAX_SWAP_COUNT:
                    self.app.set_form("wait_crew")
                    self.swap_count = MAX_SWAP_COUNT + 1
                else:
                    self.swap_count += 1
            else:
                self.app.set_form("wait_game")
                end = True
            time.sleep(0.1)
