from PyQt5.QtWidgets import QLabel, QMenu

from helpers import SETTINGS, GLOBAL_INSTANCE_DATA


def toggle_friend(name):
    if name in SETTINGS.friends:
        SETTINGS.set_friends([
            n for n in SETTINGS.friends if n.lower() != name.lower()
        ])
    else:
        SETTINGS.set_friends(
            SETTINGS.friends + [name]
        )


def toggle_enemy(name):
    if name in SETTINGS.enemies:
        SETTINGS.set_enemies([
            n for n in SETTINGS.enemies if n.lower() != name.lower()
        ])
    else:
        SETTINGS.set_enemies(
            SETTINGS.enemies + [name]
        )


class PlayerNameLabel(QLabel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.stylize()

    def stylize(self):
        player_name = self.text()
        if player_name == GLOBAL_INSTANCE_DATA["local_player_name"]:
            self.setStyleSheet("""
                QLabel {
                    color: #c7911e;
                }
            """)
        elif player_name.lower() in [name.lower() for name in SETTINGS.friends]:
            self.setStyleSheet("""
                QLabel {
                    color: #7bd179;
                }
            """)
        elif player_name.lower() in [name.lower() for name in SETTINGS.enemies]:
            self.setStyleSheet("""
                QLabel {
                    color: #d15441;
                }
            """)
        else:
            self.setStyleSheet("")

    def contextMenuEvent(self, event):
        player_name = self.text()
        context_menu = QMenu(self)
        friend_act = context_menu.addAction(f"{'Remove' if player_name in SETTINGS.friends else 'Add'} friend")
        enemy_act = context_menu.addAction(f"{'Remove from' if player_name in SETTINGS.enemies else 'Add to'} blacklist")
        action = context_menu.exec_(self.mapToGlobal(event.pos()))
        if action == friend_act:
            toggle_friend(player_name)
        elif action == enemy_act:
            toggle_enemy(player_name)
