import traceback

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel

from widgets.CrewsGrid import CrewsGrid
from helpers import SETTINGS
from widgets.ListInputForm import ListInputForm
from widgets.SotButton import SotButton, SotButtonStyle


class MainForm(QWidget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.crews = {}

        self.main_layout = QGridLayout()
        self.main_layout.setAlignment(Qt.AlignTop)

        self.title = QLabel("Players on this server :")
        self.title.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.title, 0, 0, 1, 2)

        self.subtitle = QLabel("...")
        self.subtitle.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.subtitle, 1, 0, 1, 2)

        self.crews_grid = CrewsGrid()
        self.main_layout.addWidget(self.crews_grid, 2, 0, 1, 2)

        self.friends_window = ListInputForm(
            title="Friends list",
            data=SETTINGS.friends,
            on_close=SETTINGS.set_friends
        )

        self.enemies_window = ListInputForm(
            title="Blacklisted players",
            data=SETTINGS.enemies,
            on_close=SETTINGS.set_enemies
        )

        self.friends_button = SotButton("Friends", SotButtonStyle.Green)
        self.friends_button.clicked.connect(lambda: self.friends_window.show(SETTINGS.friends))
        self.main_layout.addWidget(self.friends_button, 3, 0)

        self.enemies_button = SotButton("Blacklist", SotButtonStyle.Burgundy)
        self.enemies_button.clicked.connect(lambda: self.enemies_window.show(SETTINGS.enemies))
        self.main_layout.addWidget(self.enemies_button, 3, 1)

        self.setLayout(self.main_layout)

    def set_crews_list(self, crews):
        players_count = sum(len(crews[crew_id].players) for crew_id in crews)
        self.title.setText(
            f"{players_count} player{'s' if players_count > 1 else ''} "
            f"for {len(crews)} crew{'s' if len(crews) > 1 else ''} on this server"
        )

        available_crews = 6 - len(crews)
        available_players = 20 - sum(crews[crew_id].ship_type.value for crew_id in crews)
        if available_crews > 0 and available_players > 0:
            self.subtitle.setText(
                f"{available_players} player slot{'s' if available_players > 1 else ''} "
                f"for {available_crews} crew slot{'s' if available_crews > 1 else ''} available !"
            )
            self.subtitle.setStyleSheet("""
                color: #7bd179;
            """)
        else:
            self.subtitle.setText(
                "No crew slot available !"
            )
            self.subtitle.setStyleSheet("""
                color: #d15441;
            """)

        try:
            for old_crew_id in self.crews:
                if old_crew_id not in crews:
                    self.crews_grid.remove_crew(old_crew_id)
            for crew_id in list(crews.keys()):
                crew = crews[crew_id]
                if self.crews_grid.has_crew(crew):
                    self.crews_grid.update_crew(crew)
                else:
                    self.crews_grid.add_crew(crew)
            self.crews = crews
        except Exception as e:
            print(e)
            exit()
