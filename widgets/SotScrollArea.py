from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QScrollArea, QSizePolicy


class SotScrollArea(QScrollArea):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.setWidgetResizable(True)
        self.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.setStyleSheet("""
        * {
            background-color: transparent;
            border: 0;
        }
        QScrollBar {
            background-color: #0a1c24;
            width: 12px;
        }
        QScrollBar::vertical {
            margin-left: 5px;
        }
        QScrollBar::handle {
            background-color: #1a2c34;
            border-radius: 5px;
        }
        QScrollBar::add-page, QScrollBar::sub-page, QScrollBar::add-line, QScrollBar::sub-line {
            background: none;
        }
        """)
