from enum import Enum

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QPushButton


class SotButtonStyle(Enum):
    Blue = "blue"
    Brown = "brown"
    Burgundy = "burgundy"
    Gold = "gold"
    Green = "green"
    Orange = "orange"
    Purple = "purple"
    Red = "red"
    Turquoise = "turquoise"


class SotButtonSize(Enum):
    Small = "5px 12px"
    Medium = "15px 22px"
    Large = "25px 32px"


class SotButton(QPushButton):
    def __init__(self, text, style, *args, size=SotButtonSize.Medium, **kwargs):
        super().__init__(text, *args, **kwargs)
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.setStyleSheet(f"""
        * {{
            border: 0;
            border-image: url(':/images/buttons/{style.value}-off.svg');
            padding: {size.value};
        }}
        *:hover {{
            border-image: url(':/images/buttons/{style.value}-hover.svg');
        }}
        """)
