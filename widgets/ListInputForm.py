from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPainter, QIcon
from PyQt5.QtWidgets import QWidget, QStyleOption, QStyle, QLabel, QGridLayout, QLineEdit, QSizePolicy

from helpers import resource_path
from widgets.SotButton import SotButton, SotButtonStyle, SotButtonSize
from widgets.SotGrid import SotGrid
from widgets.SotScrollArea import SotScrollArea


class ListInputForm(QWidget):
    def __init__(self, title="", data=None, on_close=lambda data: None, **kwargs):
        super().__init__(**kwargs)
        if data is None:
            data = []
        self.on_close = on_close

        self.setWindowTitle(title)
        self.setWindowIcon(QIcon(resource_path("images/icon.ico")))
        self.resize(360, 520)
        self.setStyleSheet("""
        * {
            color: #e8e2d1;
            font-family: PragRoman;
            font-size: 16px;
        }
        ListInputForm {
            background-color: #0a1c24;
        }
        """)

        self.main_layout = QGridLayout()
        self.main_layout.setAlignment(Qt.AlignTop)

        self.title = QLabel(f"{title} :")
        self.main_layout.addWidget(self.title, 0, 0)

        self.input_list = SotGrid()
        self.input_list.grid.setAlignment(Qt.AlignTop)
        self.update_content(data)

        self.scroll_zone = SotScrollArea()
        self.scroll_zone.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.scroll_zone.setWidget(self.input_list)

        self.main_layout.addWidget(self.scroll_zone, 1, 0)

        self.add_button = SotButton("Add", SotButtonStyle.Brown)
        self.add_button.clicked.connect(lambda: self.add_entrie(""))
        self.main_layout.addWidget(self.add_button, 2, 0)

        self.setLayout(self.main_layout)

    def update_content(self, data):
        for index in range(self.input_list.grid.count())[::-1]:
            self.input_list.grid.itemAt(index).widget().deleteLater()
        for text in data:
            self.add_entrie(text)

    def add_entrie(self, text):
        text_input = QLineEdit()
        text_input.setText(text)
        text_input.setStyleSheet("""
        * {
            background-color: #0fe8e2d1;
            border: 0;
            border-bottom: 1px solid #7fe8e2d1;
            border-radius: 2px;
            padding: 5px 2px;
            margin: 3px;
         }
         *:focus {
            border-bottom: 2px solid #e8e2d1;      
         }
        """)

        def remove_entrie():
            text_input.deleteLater()
            del_button.deleteLater()

        del_button = SotButton("-", SotButtonStyle.Red, size=SotButtonSize.Small)
        del_button.clicked.connect(remove_entrie)

        row = self.input_list.grid.rowCount()
        self.input_list.grid.addWidget(text_input, row, 0)
        self.input_list.grid.addWidget(del_button, row, 1)

    def show(self, data):
        self.update_content(data)
        super().show()

    def closeEvent(self, event):
        self.on_close([
            self.input_list.grid.itemAt(i).widget().text()
            for i in range(self.input_list.grid.count())
            if isinstance(self.input_list.grid.itemAt(i).widget(), QLineEdit)
        ])
        event.accept()

    def paintEvent(self, a0):
        opt = QStyleOption()
        opt.initFrom(self)
        p = QPainter(self)
        self.style().drawPrimitive(QStyle.PE_Widget, opt, p, self)
