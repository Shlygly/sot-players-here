from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QLabel, QGridLayout, QSpacerItem, QSizePolicy, QMessageBox

from helpers import resource_path
from widgets.Loading import Loading
from widgets.SotButton import SotButton, SotButtonStyle, SotButtonSize


class WaitingCrewForm(QWidget):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.scan_thread = None

        self.main_layout = QGridLayout()
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.spacer = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.spacer, 0, 0, 1, 5)

        self.loading = Loading()
        self.main_layout.addWidget(self.loading, 1, 2)

        self.label = QLabel("Searching for players...\nDid you join a crew ?")
        self.label.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.label, 2, 0, 1, 5)

        self.spacer2 = QSpacerItem(10, 10, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.main_layout.addItem(self.spacer2, 3, 0, 1, 5)

        self.scan_button = SotButton("Scan for game updates", SotButtonStyle.Blue, size=SotButtonSize.Medium)
        self.scan_button.clicked.connect(self.launch_scan)
        self.main_layout.addWidget(self.scan_button, 4, 1, 1, 3)

        self.scan_label = QLabel("Launch a scan only if you can't see any player here")
        self.scan_label.setStyleSheet("""
            font-size: 10px;
        """)
        self.scan_label.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.scan_label, 5, 0, 1, 5)

        self.setLayout(self.main_layout)

    def set_scan_thread(self, scan_thread):
        self.scan_thread = scan_thread
        self.scan_thread.update_scan_signal.connect(self.update_scan)
        self.scan_thread.finish_scan_signal.connect(self.finish_scan)

    def launch_scan(self):
        message_box = QMessageBox()
        message_box.setWindowTitle("Scan for game updates")
        message_box.setText(
            "You're going to scan the game to let this tool access players data.\n"
            "Do this only if you keep seeing this screen even if you already joined a crew.\n"
            "The scan will only work if you have joined a crew.\n\n"
            "Launch the scan ? (It can take some time)"
        )
        message_box.setWindowIcon(QIcon(resource_path("images/icon.ico")))
        message_box.setIcon(QMessageBox.Information)
        message_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        response = message_box.exec_()

        if response == QMessageBox.Yes:
            self.scan_button.setHidden(True)
            self.scan_label.setText("Scan in progress...")
            self.scan_thread.start()

    def update_scan(self, progress):
        if progress == 1:
            self.scan_label.setText("Scan in progress (1/3) : searching for UWorld address...")
        elif progress == 2:
            self.scan_label.setText("Scan in progress (2/3) : searching for GameObject address...")
        elif progress == 3:
            self.scan_label.setText("Scan in progress (3/3) : searching for GameName address...")

    def finish_scan(self, addresses):
        message_box = QMessageBox()
        message_box.setWindowTitle("Scan complete")
        message_box.setText(
            "The scan was completed without error !"
        )
        message_box.setDetailedText(
            f"New memory addresses :\n"
            f" - UWorld : {addresses['uworld']:#08x}\n"
            f" - GameObject : {addresses['gobject']:#08x}\n"
            f" - GameName : {addresses['gname']:#08x}"
        )
        message_box.setWindowIcon(QIcon(resource_path("images/icon.ico")))
        message_box.setIcon(QMessageBox.Information)
        message_box.exec_()

        self.scan_button.setHidden(False)
        self.scan_label.setText("Launch a scan only if you can't see any player here")
