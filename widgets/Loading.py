from PyQt5.QtCore import QVariantAnimation, pyqtSlot, QVariant, Qt
from PyQt5.QtGui import QTransform, QPixmap
from PyQt5.QtWidgets import QLabel

from helpers import resource_path

SIZE = 120


class Loading(QLabel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setMinimumSize(SIZE, SIZE)
        self.setMaximumSize(SIZE, SIZE)
        self.setAlignment(Qt.AlignCenter)

        self.img = QPixmap(resource_path("images/wheel.svg")).scaled(SIZE * .9, SIZE * .9, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        self.setPixmap(self.img)

        self.animation = QVariantAnimation(self)
        self.animation.setStartValue(0.0)
        self.animation.setEndValue(360.0)
        self.animation.setDuration(5000)
        self.animation.setLoopCount(-1)
        self.animation.valueChanged.connect(self.on_rotate)
        self.animation.start()

    @pyqtSlot(QVariant)
    def on_rotate(self, value):
        t = QTransform()
        t.rotate(value)
        self.setPixmap(self.img.transformed(t, Qt.SmoothTransformation))
