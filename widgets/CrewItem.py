from PyQt5.QtCore import Qt
from PyQt5.QtMultimedia import QSound
from PyQt5.QtWidgets import QGridLayout, QLabel, QFrame

from helpers import resource_path, SETTINGS
from widgets.PlayerNameLabel import PlayerNameLabel


class CrewItem(QFrame):
    def __init__(self, crew, **kwargs):
        super().__init__(**kwargs)
        self.players = {}

        self.sounds = {
            "any-join": QSound(resource_path("sounds/any-join.wav")),
            "any-left": QSound(resource_path("sounds/any-left.wav")),
            "friend-join": QSound(resource_path("sounds/friend-join.wav"))
        }

        self.main_layout = QGridLayout()

        self.header = QLabel(f"{crew.ship_type.name} - {len(crew.players)}/{crew.ship_type.value}")
        self.header.setAlignment(Qt.AlignCenter)
        self.header.setStyleSheet("""
            font-size: 12px;
            color: #e8d7a7;
        """)
        self.main_layout.addWidget(self.header, 0, 0, 1, 2)

        self.setLayout(self.main_layout)

        for player in crew.players:
            self.add_player(player)

    def update_crew(self, crew):
        self.header.setText(f"{crew.ship_type.name} - {len(crew.players)}/{crew.ship_type.value}")
        new_players_names = [p.name for p in crew.players]
        for old_player_name in list(self.players.keys()):
            if old_player_name not in new_players_names:
                self.remove_player(old_player_name)
        for player in crew.players:
            if self.has_player(player):
                self.update_player(player)
            else:
                self.add_player(player)

    def remove_crew(self):
        for name in self.players:
            self.remove_player(name)

    def add_player(self, player):
        if self.has_player(player):
            self.update_player(player)
        else:
            name_label = PlayerNameLabel(player.name)

            activity_label = QLabel()
            activity_label.setAlignment(Qt.AlignRight | Qt.AlignCenter)
            activity_label.setPixmap(player.get_activity_icon())

            row_index = self.main_layout.rowCount()
            self.main_layout.addWidget(name_label, row_index, 0)
            self.main_layout.addWidget(activity_label, row_index, 1)

            self.players[player.name] = {
                "state": player,
                "widgets": {
                    "name": name_label,
                    "activity": activity_label
                }
            }

            if player.name in SETTINGS.friends:
                self.sounds["friend-join"].play()
            else:
                self.sounds["any-join"].play()

    def has_player(self, player):
        return player.name in self.players

    def update_player(self, player):
        if not self.has_player(player):
            self.add_player(player)
        else:
            self.players[player.name]["widgets"]["name"].stylize()
            self.players[player.name]["widgets"]["activity"].setPixmap(player.get_activity_icon())

    def remove_player(self, name):
        for key in self.players[name]["widgets"]:
            self.main_layout.removeWidget(self.players[name]["widgets"][key])
        del self.players[name]

        self.sounds["any-left"].play()
