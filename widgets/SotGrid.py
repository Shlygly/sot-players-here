from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QGridLayout, QStyleOption, QStyle, QFrame


class SotGrid(QFrame):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setStyleSheet("""
        SotGrid {
            border-image: url(':/images/card-shape.svg');
            padding: 20px;
        }
        * {
            font-size: 14px;
        }
        """)

        self.grid = QGridLayout()
        self.setLayout(self.grid)

    def paintEvent(self, a0):
        opt = QStyleOption()
        opt.initFrom(self)
        p = QPainter(self)
        self.style().drawPrimitive(QStyle.PE_Widget, opt, p, self)

