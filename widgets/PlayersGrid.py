from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel

from helpers import SETTINGS
from widgets.SotGrid import SotGrid


def stylize_label(label, player):
    if player.name in SETTINGS.friends:
        label.setStyleSheet("""
            color: #7bd179;
        """)
    elif player.name in SETTINGS.enemies:
        label.setStyleSheet("""
            color: #d15441;
        """)


class PlayersGrid(SotGrid):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.players = {}
        title_style = """
            font-weight: bold;
        """

        title_username = QLabel("Username")
        title_username.setStyleSheet(title_style)

        title_activity = QLabel("Action")
        title_activity.setAlignment(Qt.AlignCenter)
        title_activity.setStyleSheet(title_style)

        title_ping = QLabel("Ping")
        title_ping.setAlignment(Qt.AlignCenter)
        title_ping.setStyleSheet(title_style)

        title_das = QLabel("Time at sea")
        title_das.setAlignment(Qt.AlignRight | Qt.AlignCenter)
        title_das.setStyleSheet(title_style)

        self.grid.addWidget(title_username, 0, 0)
        self.grid.addWidget(title_activity, 0, 1)
        self.grid.addWidget(title_ping, 0, 2)
        self.grid.addWidget(title_das, 0, 3)

        separator = QLabel()
        separator.setMaximumHeight(18)
        separator.setStyleSheet("""
            border-image: url(:/images/separator.svg);
            margin: 6px 0;
        """)
        self.grid.addWidget(separator, 1, 0, 1, 4)

    def add_player(self, player):
        if self.has_player(player):
            self.update_player(player)
        else:
            name_label = QLabel(player.name)
            stylize_label(name_label, player)

            activity_label = QLabel()
            activity_label.setAlignment(Qt.AlignCenter)
            activity_label.setPixmap(player.get_activity_icon())

            ping_label = QLabel(f"{player.ping}ms" if player.ping else "")
            ping_label.setAlignment(Qt.AlignCenter)

            das_label = QLabel("")
            das_label.setAlignment(Qt.AlignRight | Qt.AlignCenter)

            row_index = self.grid.rowCount()
            self.grid.addWidget(name_label, row_index, 0)
            self.grid.addWidget(activity_label, row_index, 1)
            self.grid.addWidget(ping_label, row_index, 2)
            self.grid.addWidget(das_label, row_index, 3)

            self.players[player.name] = {
                "state": player,
                "widgets": {
                    "name": name_label,
                    "activity": activity_label,
                    "ping": ping_label,
                    "days_at_sea": das_label
                }
            }

            def das_update(days_at_sea):
                if days_at_sea is not None and player.name in self.players and "dats_at_sea" in self.players[player.name]:
                    self.players[player.name]["days_at_sea"].setText(
                        f"{days_at_sea} day{'s' if days_at_sea > 1 else ''}"
                        if days_at_sea is not None else
                        ""
                    )

            player.set_info_update(das_update)

    def has_player(self, player):
        return player.name in self.players

    def update_player(self, player):
        if not self.has_player(player):
            self.add_player(player)
        else:
            # self.players[player.name]["state"] = player
            stylize_label(self.players[player.name]["widgets"]["name"], player)
            self.players[player.name]["widgets"]["activity"].setPixmap(player.get_activity_icon())
            self.players[player.name]["widgets"]["ping"].setText(f"{player.ping}ms" if player.ping else "")

    def remove_player(self, name):
        for key in self.players[name]["widgets"]:
            self.grid.removeWidget(self.players[name]["widgets"][key])
        del self.players[name]
