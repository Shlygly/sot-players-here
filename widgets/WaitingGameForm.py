from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QLabel, QGridLayout

from widgets.SotButton import SotButton, SotButtonStyle


class WaitingGameForm(QWidget):
    def __init__(self, on_refresh=lambda *a, **k: None, **kwargs):
        super().__init__(**kwargs)
        self.main_layout = QGridLayout()
        self.main_layout.setAlignment(Qt.AlignCenter)

        self.label = QLabel(f"Waiting for Sea of thieves...")
        self.label.setAlignment(Qt.AlignCenter)
        self.main_layout.addWidget(self.label, 0, 0, 1, 3)

        self.button = SotButton("Refresh", SotButtonStyle.Turquoise)
        self.button.clicked.connect(on_refresh)
        self.main_layout.addWidget(self.button, 1, 1)

        self.setLayout(self.main_layout)

    def set_on_refresh(self, on_refresh):
        self.button.clicked.connect(on_refresh)
