from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel

from widgets.CrewItem import CrewItem
from widgets.SotGrid import SotGrid


class CrewsGrid(SotGrid):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.crews = {}
        title_style = """
            font-weight: bold;
        """

        self.grid.setSpacing(0)

        title_username = QLabel("Username")
        title_username.setStyleSheet(title_style)

        title_activity = QLabel("Action")
        title_activity.setAlignment(Qt.AlignRight | Qt.AlignCenter)
        title_activity.setStyleSheet(title_style)

        separator = QLabel()
        separator.setMaximumHeight(18)
        separator.setStyleSheet("""
            border-image: url(:/images/separator.svg);
            margin: 6px 0;
        """)

        self.grid.addWidget(title_username, 0, 0)
        self.grid.addWidget(title_activity, 0, 1)
        self.grid.addWidget(separator, 1, 0, 1, 2)

    def add_crew(self, crew):
        if self.has_crew(crew):
            self.update_crew(crew)
        else:
            crew_item = CrewItem(crew)
            self.grid.addWidget(crew_item, self.grid.rowCount(), 0, 1, 2)

            separator = QLabel()
            separator.setMaximumHeight(6)
            separator.setStyleSheet("""
                border-image: url(:/images/separator.svg);
                margin: 0;
            """)
            self.grid.addWidget(separator, self.grid.rowCount(), 0, 1, 2)

            self.crews[crew.id] = {
                "separator": separator,
                "item": crew_item
            }

    def has_crew(self, crew):
        return crew.id in self.crews

    def update_crew(self, crew):
        if not self.has_crew(crew):
            self.add_crew(crew)
        else:
            self.crews[crew.id]["item"].update_crew(crew)

    def remove_crew(self, crew_id):
        for key in self.crews[crew_id]:
            if self.crews[crew_id][key]:
                self.grid.removeWidget(self.crews[crew_id][key])
        del self.crews[crew_id]
