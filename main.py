import sys

from PyQt5.QtGui import QFontDatabase, QIcon
from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QScrollArea, QSizePolicy

import resources
from MainThread import MainThread
from ProcessNotFoundError import ProcessNotFoundError
from ScanThread import ScanThread
from helpers import resource_path
from sot_hack import SoTMemoryReader
from widgets.MainForm import MainForm
from widgets.SotScrollArea import SotScrollArea
from widgets.WaitingCrewForm import WaitingCrewForm
from widgets.WaitingGameForm import WaitingGameForm


class App(QApplication):
    sot_memory = None
    main_thread = None
    scan_thread = None

    def __init__(self):
        super().__init__(sys.argv)
        QFontDatabase.addApplicationFont(resource_path("fonts/PragRoman.ttf"))

        self.forms = {
            "wait_game": WaitingGameForm(on_refresh=self.on_game_refresh_press),
            "wait_crew": WaitingCrewForm(),
            "main": MainForm()
        }
        self.stack = QStackedWidget()
        self.forms_stack_index = {}
        for key in self.forms:
            self.forms_stack_index[key] = self.stack.addWidget(self.forms[key])

        self.scroll = SotScrollArea()
        self.scroll.setWidget(self.stack)

        self.main_window = QMainWindow()
        self.main_window.resize(520, 720)
        self.main_window.setWindowTitle("Sea of thieves - Who's there")
        self.main_window.setWindowIcon(QIcon(resource_path("images/icon.ico")))
        self.main_window.setCentralWidget(self.scroll)
        self.main_window.setStyleSheet("""
        * {
            color: #e8e2d1;
            font-family: PragRoman;
            font-size: 14px;
        }
        QMainWindow {
            background-color: #0a1c24;
        }
        """)

        self.set_form("wait_game")
        self.on_game_refresh_press()

        self.main_window.show()

    def set_form(self, key):
        index = self.forms_stack_index[key]
        if index != self.stack.currentIndex():
            self.stack.setCurrentIndex(index)

    def on_game_refresh_press(self):
        try:
            self.sot_memory = SoTMemoryReader()
            self.set_form("wait_crew")
            self.main_thread = MainThread(self)
            self.main_thread.set_crews_signal.connect(self.forms["main"].set_crews_list)
            self.main_thread.start()
        except ProcessNotFoundError:
            self.sot_memory = None
        finally:
            self.scan_thread = ScanThread(self)
            self.forms["wait_crew"].set_scan_thread(self.scan_thread)


if __name__ == '__main__':
    sys.exit(App().exec_())
